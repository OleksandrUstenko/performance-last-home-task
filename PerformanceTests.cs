using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace PerfHT
{
    public class Tests
    {
        private static IWebDriver driver;
        protected readonly long timeout = 3000;
        private IWait<IWebDriver> wait => new WebDriverWait(driver, TimeSpan.FromMilliseconds(3000));

        [SetUp]
        public void Setup()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--start-maximized");
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), options);
            driver.Navigate().GoToUrl("https://demo.nopcommerce.com/");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
            driver.Quit();
            driver = null;
        }

        private IWebElement ComputersButton => driver.FindElement(By.XPath("//ul[@class='top-menu notmobile']//a[contains(text(),\"Computers\")]"));

        private IWebElement DesktopsButton => driver.FindElement(By.XPath("//h2//a[@title='Show products in category Desktops']"));

        private IWebElement DigitalStormDesctopButton => driver.FindElement(By.XPath("//a[contains(text(),\"Digital Storm\")]"));

        public void WaitForPageLoadComplete()
        {
            try
            { 
                wait.Until(driver1 => ((IJavaScriptExecutor)driver)
                .ExecuteScript("return document.readyState").Equals("complete"));
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
        }

        public void GoToComputersPage()
        {
            ComputersButton.Click();
            WaitForPageLoadComplete();
        }

        public void GoToDesktopsPage()
        {
            //as element is not interactable
            Thread.Sleep(500);
            DesktopsButton.Click();
            WaitForPageLoadComplete();
        }

        public void GoToDigitalStormDesctopPage()
        {
            DigitalStormDesctopButton.Click();
            WaitForPageLoadComplete();
        }

        [Test]
        public void GetMetric_ComputersPage()
        {
            GoToComputersPage();
           var currentMetrics = writePerfMetricasJSON("computers");

           Assert.Less(Convert.ToInt32(currentMetrics.PageLoadTime), 500, "Page load took a lot of time");
        }

        [Test]
        public void GetMetric_DesktopsPage()
        {
            GoToComputersPage();
            GoToDesktopsPage();
            var currentMetrics = writePerfMetricasJSON("desktops");

            Assert.That(currentMetrics.Url.Contains("desktops"), "Wrong url is present");
        }

        [Test]
        public void GetMetric_CertainDesktopPage()
        {
            GoToComputersPage();
            GoToDesktopsPage();
            GoToDigitalStormDesctopPage();
            var currentMetrics = writePerfMetricasJSON("digitalStormDesktop");

            Assert.Less(Convert.ToInt32(currentMetrics.LatencyTime), 1500, "Latency time is more then 1500");
        }
        
        public static PerfData writePerfMetricasJSON(string fileName)
		{
			IJavaScriptExecutor javaScriptExecutor = (IJavaScriptExecutor)driver;

			string url = driver.Url;
			Console.WriteLine("Current URL :" + url);
			long pageLoadTime = Convert.ToInt32(javaScriptExecutor.ExecuteScript("return (window.performance.timing.loadEventEnd-window.performance.timing.responseStart)"));
			long latencyTime = Convert.ToInt32(javaScriptExecutor.ExecuteScript("return (window.performance.timing.responseStart-window.performance.timing.navigationStart)"));
			long totalTime = Convert.ToInt32(javaScriptExecutor.ExecuteScript("return (window.performance.timing.loadEventEnd-window.performance.timing.navigationStart)"));

			Console.WriteLine("Page Load Time:" + pageLoadTime);
			Console.WriteLine("Latency Time:" + latencyTime);
			Console.WriteLine("Total Time:" + totalTime);


			PerfData currentMetrics = new PerfData() { 
			Url = url,
            TotalTime = totalTime.ToString(),
			PageLoadTime = pageLoadTime.ToString(),
            LatencyTime = latencyTime.ToString()};

			try
			{
				var jsoncontent = JsonConvert.SerializeObject(currentMetrics);
               
                var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)+@"\..\..\..\Results\" + fileName + ".json";

                File.WriteAllText(path, jsoncontent);

                
			}
			catch (Exception e) { Console.WriteLine(e.Message); }

            return currentMetrics;
		}
	}

	public class PerfData
	{
		public string Url { get; set; }
		public string PageLoadTime { get; set; }
		public string LatencyTime { get; set; }
		public string TotalTime { get; set; }
	}
}